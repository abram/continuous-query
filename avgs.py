try:
   import cPickle as pickle
except:
   import pickle
import argparse
import os
import os.path
import sys
import numpy as np
import logging

def init_basic_logging(logfile="avgs.log"):
    logging.basicConfig(filename=logfile, stream = sys.stderr, level=logging.INFO)
    # stderr out
    logging.getLogger().addHandler(logging.StreamHandler())

def parse_args(add_args=None):
    parser = argparse.ArgumentParser(description='Stats on Runs')
    parser.add_argument('--notordered',action='store_true',help="Follow strict ordered output of tex file")
    parser.add_argument('--prefix',default="",help="Prefix")
    parser.add_argument('--suffix',default="",help="Suffix to average files")
    parser.add_argument('files', help='Filenames',nargs='*')
    if (add_args!=None):
        add_args(parser)
    args = parser.parse_args()
    return args

init_basic_logging()
 
ourkeys = ['MAP', 'MRR', 'TOP1', 'TOP5', 'TOP10', 'aveP-TOP5', 'first-TOP5']

args = parse_args()
print args
prefix = args.prefix
suffix = args.suffix
if len(args.files) == 0:
   import projects
   projects = projects.Projects()
   files = ["%s/%s_issues.json/%scontquery.pkl" % (kind,project,prefix) for project in projects.projects for kind in ["exp1","expnostem"]]
   print files
   args.files = files

print args.files

def get_name(name):
   return [x.split("/")[-1] for x in name.split(".") if "issue" in x][0]

fd = file("%saverages.csv" % prefix,"w")

def write_out(fd,str):
   print str
   fd.writelines([str,"\n"])
import projects
projects = projects.Projects()
# header
write_out(fd,",".join(["filename","shortname","project","exp"] + ourkeys + ["i1stTOP5"]))

exps = {
   "exp1":"Stemming",
   "expnostem":"NoStem"
}

def get_exp(filename):
   v = [x for x in filename.split("/") if x in exps.keys()]
   if len(v) == 0:
      return ""
   else:
      return v[0]

      
results = dict()
avgs = list()
for filename in args.files:
   res = pickle.load(file(filename))
   results[filename] = res
   sname = get_name(filename)
   sname = sname.replace("_issues","")
   name = projects.get_name(sname)
   my_exp = get_exp(filename)
   def calc_avg(kind):
      avgs = [x["avgs"].get(kind,0.0) for x in res["output"]]
      #return format(sum(avgs)/float(len(avgs)), '%<01.3f')
      return np.nanmean(avgs)
   try:
      l = [filename,sname,name,exps[my_exp]] + [calc_avg(x) for x in ourkeys]
      l.append(1.0/l[-1])
      avgs.append(l)
      s = list(l)
      for i in range(3,len(l)):
         s[i] = str(l[i])
      write_out(fd,",".join(s))         
   except KeyError as e:
      print filename
      print res['avgavg']
      print res
      print e
fd.close()

def key_gen(l):
   proj = l[2]
   expr = l[3]
   if (expr == "exp1" or expr == exps["exp1"]):
      expr = "A Stemming"
   return proj + " " + expr

if not args.notordered:
   avgs = sorted(avgs,key=key_gen)


def fmtavg(x):
   if (len(x) == 0):
       return '0.000'
   return format(sum(x)/float(len(x)),'%<01.3f')

exp1line = " & ".join(["",exps["exp1"]] + [fmtavg([l[j] for l in avgs if l[3] == exps["exp1"]]) for j in range(4,len(avgs[0]))])
expnostemline = " & ".join(["",exps["expnostem"]] + [fmtavg([l[j] for l in avgs if l[3] != exps["exp1"]]) for j in range(4,len(avgs[0]))])
lastline = " & ".join(["","Both"] + [fmtavg([l[j] for l in avgs]) for j in range(4,len(avgs[0]))])


fd = file("%saverages.tex"%prefix,"w")
write_out(fd," & ".join(["Project","Exp"] + ourkeys + ["1/1stTop5"]) + " \\\\")
for l in avgs:
   #l.append(1.0/l[-1]) # reciprocal line
   for i in range(4,len(l)):
      l[i] = format(l[i], '%<01.3f')
   l = l[2:]
   write_out(fd," & ".join(l) + " \\\\")

   
write_out(fd, "%s \\\\" % exp1line)
write_out(fd, "%s \\\\" % expnostemline)
write_out(fd, "%s \\\\" % lastline)
fd.close()
   
for kind in ourkeys:
   # res = pickle.load(file(filename))
   for filename in args.files:
      res = results[filename]
      name = get_name(filename)
      out = "%s/%s%s.%s%s.csv" % (os.path.dirname(filename), prefix, name, kind,suffix)
      avgs = [x["avgs"].get(kind,0.0) for x in res["output"]]
      fd = file(out,"w")
      fd.writelines("%s\n"%x for x in avgs)
      fd.close()

