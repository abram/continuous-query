import climate
import numpy
import numpy as np
import nltk
import matplotlib.pyplot as plt
import sys
import logging
from nltk.tokenize import regexp_tokenize, wordpunct_tokenize

import matplotlib.pyplot as plt
import scipy
import scipy.spatial
import scipy.spatial.distance


def replace_mutate(a,n=1):
    words = wordpunct_tokenize(a)
    for i in range(0,n):
        words[np.random.randint(0,len(words))] = ogwords[np.random.randint(0,len(ogwords))]
    return " ".join(words)

def delete_mutate(a,n=1):
    words = wordpunct_tokenize(a)
    i = 0
    while(len(words) > 0 and i < n):
        del words[np.random.randint(0,len(words))]
        i += 1
    return " ".join(words)

def add_mutate(a,n=1):
    words = wordpunct_tokenize(a)
    for i in range(0,n):
        words.append(ogwords[np.random.randint(0,len(ogwords))])
    return " ".join(words)
