for b in 0.0 0.1 0.2 0.25 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0
do
	python contquery.py -i data/appinventorforandroid_issues.json --bm25 --bp expnostem/appinventorforandroid_issues.json/bp.pkl --runcont --parts 100 --log expnostem/appinventorforandroid_issues.json/contquery.bm25.$b.log --contquerypkl expnostem/appinventorforandroid_issues.json/contquery.bm25.$b.pkl --nostem --bm25b $b --bm25eps 0.25 --bm25k1 1.25 &
done
wait
python avgs.py expnostem/appinventorforandroid_issues.json/contquery.bm25.*.pkl
cp averages.csv averages.bm25.tuning.csv
