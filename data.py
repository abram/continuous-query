try:
   import cPickle as pickle
except:
   import pickle
import argparse
import os
import os.path
import sys
import logging
import projects
import bug
import contquery
import IRExp

def init_basic_logging(logfile="data.log"):
    logging.basicConfig(filename=logfile, stream = sys.stderr, level=logging.INFO)
    # stderr out
    logging.getLogger().addHandler(logging.StreamHandler())

init_basic_logging()

projects = projects.Projects()

model = IRExp.make_a_good_ir()

def average(x):
    return sum(x)/float(len(x))

def average_title_words(bc):
    lengths = [len(model.process_document(bug.get_title())) for bug in bc.bugs]
    return average(lengths)

def average_description_words(bc):
    lengths = [len(model.process_document(bug.get_description())) for bug in bc.bugs]
    return average(lengths)


def decfmt(x):
    return format(x,'%<01.3f')

for project in projects.projects:
    json_file = projects.get_json_file(project)
    bugs = bug.load_bugs(json_file)
    bc = contquery.BugContext( bugs )
    n_issues = bc.len()
    n_dupes  = bc.n_dupe_issues()
    n_buckets = bc.n_buckets()
    title_len = bc.average_title_length()
    desc_len = bc.average_description_length()
    title_words = average_title_words(bc)
    desc_words = average_description_words(bc)

    (earliest,latest) = bc.date_range()
    print " & ".join([projects.get_name(project),"",str(n_issues),str(n_dupes),str(n_buckets),decfmt(title_words), decfmt(desc_words),str(earliest),str(latest)]) + " \\\\"

