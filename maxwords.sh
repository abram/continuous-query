PROJECT=$1
mkdir exp3/||echo done
mkdir exp3/$PROJECT||echo done
counts="1 5 10 15 20 25 50 100 200 400 800 1600 3200"
# 6400"
for file in $counts
do
	python contquery.py -i data/$PROJECT --bp exp3/$PROJECT/bp.pkl --runcont --parts 10 --words $file --log exp3/$PROJECT/contquery.words.$file.log --contquerypkl exp3/$PROJECT/contquery.words.$file.pkl
done

