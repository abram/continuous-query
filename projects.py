import json

class Projects(object):    
    project_dict = None
    projects = None
    project_names = None
    def __init__(self):
        if Projects.project_dict == None:
            Projects.project_dict = json.load(file("projects.json"))
            Projects.projects = sorted(Projects.project_dict.keys())
            Projects.project_names = [Projects.project_dict[name] for name in Projects.projects]
            
    def get_name(self,project):
        return self.project_dict[project]
    def get_json_file(self, project):
        return "data/%s_issues.json" % project
