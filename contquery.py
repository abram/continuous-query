import json
try:
   import cPickle as pickle
except:
   import pickle

import nltk
import ireval
import argparse
import logging
import numpy as np
import scipy
import scipy.spatial
import scipy.spatial.distance
import os
import gensim
import gensim.models.tfidfmodel
import gensim.similarities.docsim
import gensim.summarization.bm25 as bm25
import altscheme
import altbm25
import tempfile
import sys
import bug
import IRExp
from math import ceil
import random
import shutil
import dateutil.parser
import urllib2
import urllib
import string
import time

# ws_tokenizer = nltk.tokenize.RegexpTokenizer('\w+|\$[\d\.]+|\S+')
ws_tokenizer = IRExp.WordSplitter()

def init_basic_logging(logfile="contquery.log"):
    logging.basicConfig(filename=logfile, stream = sys.stderr, level=logging.INFO)
    # stderr out
    logging.getLogger().addHandler(logging.StreamHandler())


def parse_args(add_args=None):
    parser = argparse.ArgumentParser(description='Continuous Query')
    parser.add_argument('-i', default="large.json", help='Input Bug JSON')
    parser.add_argument('--bp', default='bp.pkl', help="Dump temp")
    parser.add_argument('--runcont', action='store_true', help="run contquery")
    parser.add_argument('--parts', default=10, help="Partitions")
    parser.add_argument('--words', default=25, help="max_words: maximum number of words to use for continuous query")
    parser.add_argument('--log', default="contquery.log", help="Logfile")
    parser.add_argument('--contquerypkl', default='contquery.pkl', help="ContQuery Pickle")
    parser.add_argument('--nostopwords', action='store_true', help="Don't strip stopwords")
    parser.add_argument('--nostem', action='store_true', help="Don't run porterstemmers")
    parser.add_argument('--nopunct', action='store_true', help="Strip Punctuation")
    parser.add_argument('--keepdigits', action='store_true', help="Don't Strip Digits")
    parser.add_argument('--nolowercase', action='store_true', help="Don't force lowercase")
    parser.add_argument('--notcont', action='store_true', help="Don't use continuous query")
    parser.add_argument('--test', action='store_true', help="Just run tests")
    parser.add_argument('--bugparty', action='store_true', help="BugParty Tests")
    parser.add_argument('--bm25', action='store_true', help="Use BM25")
    parser.add_argument('--bm25k1',default=1.25,help="BM25 K1 parameter (1.25)")
    parser.add_argument('--bm25b',default=0.7,help="BM25 b parameter (0.5)")
    parser.add_argument('--bm25eps',default=0.25,help="BM25 epsilon (0.25)")    
    parser.add_argument('--justidf',action='store_true',help="Just IDF")
    parser.add_argument('--justtfidf',action='store_true',help="Just TFIDF")
    parser.add_argument('--justtitles',action='store_true',help="Just use titles not description")
    parser.add_argument('--nwords', action='store_true', help="Don't use continuous query -- just use max_words words")

    # parser.add_argument('files', help='Filenames',nargs='+')
    parser.set_defaults(parts=10)
    if (add_args!=None):
        add_args(parser)
    args = parser.parse_args()
    args.parts = int(args.parts)

    # settings
    bm25.PARAM_K1 = float(args.bm25k1)
    bm25.PARAM_B  = float(args.bm25b)
    bm25.EPSILON  = float(args.bm25eps)

    if args.justtitles:
       logging.info("Using just title")
       bug.Bug.desc = bug.Bug.just_title_desc
    
    return args

                         
def http_request(method, uri, data=""):
   request = urllib2.Request(uri, data=data)
   request.get_method = lambda: method
   return urllib2.urlopen(request)

def POST(uri,data=""):
   http_request("POST",uri,data)
   
def DELETE(uri,data=""):
   http_request("DELETE",uri,data)

def PUT(uri,data=""):
   http_request("PUT",uri,data)

def GET(uri,data=""):
   return http_request("GET",uri,data)
   
premature_optimization = dict()
MAX_CACHE=1000
def jsonget( uri, retries=3 ):
   if not uri in premature_optimization:
      try:
         req = urllib2.urlopen(uri, None, 100)
      except urllib2.HTTPError as e:
         if retries > 0 and (e.getcode() == 503):
            logging.info("Waiting 30 seconds before retry")
            time.sleep(30)
            return jsonget(uri, retries=retries-1)
         else:
            raise e
      j = json.loads(req.read())
      if len(premature_optimization) > MAX_CACHE:
         logging.info("Dropping 1/2 the cache!")
         while len(premature_optimization) > MAX_CACHE/2:
            del(premature_optimization[random.choice(premature_optimization.keys())])
      premature_optimization[uri] = j
   return premature_optimization[uri]

   
   
class BugContext(object):
   
   def __init__(self, my_bugs):
      try:
         my_bugs = sorted(my_bugs,key=lambda x: int(x.get_id()))
      except ValueError:
         # just sort as string
         logging.warn("Couldn't convert IDs to integers, now sorting as string")
         my_bugs = sorted(my_bugs,key=lambda x: str(x.get_id()))
      self.ids = [bug.get_id() for bug in my_bugs]
      self.dup_ids = [bug.dup_id() for bug in my_bugs]
      # self.texts = [bug.text() for bug in my_bugs]
      self.bugs = my_bugs
      self.dup_ids = np.array(self.dup_ids)
      self.registered_dupes = set(self.dup_ids)
      if "" in self.registered_dupes:
         self.registered_dupes.remove("")
      self.reverse_id = dict()
      for i in xrange(0,len(self.ids)):
         self.reverse_id[self.ids[i]] = i
         
   def is_dupe(self, bugid):
      return bugid in self.registered_dupes
   def average_title_length(self):
      return sum((len(bug.get_title()) for bug in self.bugs))/len(self.bugs)
   def average_description_length(self):
      return sum((len(bug.get_description()) for bug in self.bugs))/len(self.bugs)
   def date_range(self):
      dates = [bug.get_published() for bug in self.bugs]
      dates = [date for date in dates if date!="" and date!=None]
      if len(dates) == 0:
         return (None,None)
      pdates = [dateutil.parser.parse(date) for date in dates]
      pdates.sort()
      return (pdates[0],pdates[-1])

   def n_dupe_issues(self):
      return len([dupid for dupid in self.dup_ids if dupid!=None and dupid!=""])
   def n_buckets(self):
      return len(self.registered_dupes)
   def dupes_i_of(self, i):
      if i == None:
         return []
      actual_id = self.ids[i]
      dupe_id = self.dup_ids[i]
      if dupe_id == '' or dupe_id == None:
         dupe_id = actual_id
      return [i for i in range(0,len(self.ids)) if self.dup_ids[i] == dupe_id]

   def dupes_of_bug(self, bug):
      bid = bug.get_id()
      dupe_id = bug.dup_id()
      if dupe_id == '' or dupe_id == None:
         dupe_id = bid
      return [i for i in range(0,len(self.ids)) if self.dup_ids[i] == dupe_id]

   def dupes_of(self, bugid):
      i = self.reverse_id.get(bugid,None)
      return self.dupes_i_of(i)

   def len(self):
      return len(self.ids)

   def make_instance(self, bugs):
      return BugContext(bugs)

   def make_model(self,irmodel,bugs):
      return Model(irmodel=irmodel,bugs=bugs)
   
class BPBugContext(BugContext):
   def make_instance(self, bugs):
      return BPBugContext(bugs)
   def make_model(self,irmodel,bugs):
      return BPModel(irmodel=irmodel,bugs=bugs)

class BM25BugContext(BugContext):
   def make_instance(self, bugs):
      return BM25BugContext(bugs)
   def make_model(self,irmodel,bugs):
      return BM25Model(irmodel=irmodel,bugs=bugs)

class IDFBugContext(BugContext):
   def make_instance(self, bugs):
      return IDFBugContext(bugs)
   def make_model(self,irmodel,bugs):
      return IDFModel(irmodel=irmodel,bugs=bugs)

class TFIDFBugContext(BugContext):
   def make_instance(self, bugs):
      return TFIDFBugContext(bugs)
   def make_model(self,irmodel,bugs):
      return TFIDFModel(irmodel=irmodel,bugs=bugs)



def noneTo(v,out=[]):
   if v is None:
      return out
   return v

class Model(object):
   
   def __init__(self,irmodel=None,bugs=None):
      if irmodel is None:
         logging.info("Making a new Default IR for the model")
         self.irmodel = IRExp.make_a_good_ir()
      else:
         self.irmodel = irmodel
      if bugs is not None:
         self.train_on_docs(bugs)
   
   def train_on_docs(self,bugs):
      logging.info("Training of bugs!")
      self.irmodel.build_corpus(bugs)
      words = self.irmodel.get_words()
      logging.info("Making a bug")
      dictionary = gensim.corpora.Dictionary([words])
      self.dictionary = dictionary
      # get the corpus from the IR model and convert to bag-of-words
      corpus = [dictionary.doc2bow(noneTo(doc)) for doc in self.irmodel.get_corpus() if doc is not None]
      self.corpus = corpus
      logging.info("Making a TFIDF Model")
      tfidf = gensim.models.tfidfmodel.TfidfModel(corpus)
      self.tfidf = tfidf
      logging.info("Making a TFIDF Corpus")
      tfidfcorp = [tfidf[doc] for doc in corpus]
      self.tfidfcorp = tfidfcorp
      mydir = tempfile.mkdtemp()
      self.mydir = mydir
      logging.info("Making a similarity index")
      index = gensim.similarities.docsim.Similarity(mydir, tfidfcorp, dictionary.num_pos)
      self.index = index

   def get_words_seq(self,doc):
      """ Don't produce a bag of words, produce a sequence! """
      words = noneTo(self.irmodel.process_document(doc))
      return words

   def get_raw_words_seq(self,doc):
      """ Don't produce a bag of words, produce a sequence! """
      # words = nltk.tokenize.word_tokenize(doc)
      words = ws_tokenizer.split(doc)
      return words
      
   def get_words(self,doc):
      """ Produce a bag of words """
      words = self.dictionary.doc2bow(noneTo(self.irmodel.process_document(doc)))
      return words
   
   def similar_from_text(self,doc):
      return self.similar(self.get_words(doc))

   def similar_from_words(self,words):
      return self.similar_from_text(" ".join(words))
      return self.similar(self.dictionary.doc2bow(noneTo(words)))
   
   def similar(self,words):
      tfidfdoc = self.tfidf[words]
      distances = self.index[tfidfdoc]
      return distances

   def clean(self):
      logging.info("Removing %s" % self.mydir)
      shutil.rmtree(self.mydir)
      os.system("rm " + self.mydir + ".[0-9]")

class BP(object):
   BASE = "http://127.0.0.1:8080"
   ESBASE = "http://127.0.0.1:9200"
   def create_project(self,projectname):
      POST(self.BASE + "/bugparty/" + projectname)
   def remove_project(self,projectname):
      DELETE(self.BASE + "/bugparty/" + projectname)
   def add_bug(self,projectname, bugid, title, description):
      POST(self.BASE + "/bugparty/"+projectname+"/bugs", json.dumps({"bugid":bugid, "title":title, "description":description}))
   def get_bug(self,projectname, bugid):
      uri = self.BASE + "/bugparty/"+projectname+"/bugs/"+str(bugid)
      logging.debug("Retrieving bug %s" % uri)               
      return jsonget(uri)
   def similar_bugs(self,projectname, text):
      uri = self.BASE + "/bugparty/"+projectname+"/search_description?" + urllib.urlencode({"q":text})
      logging.debug("Similar bug %s" % uri)               
      return jsonget(uri)
   def refresh(self,projectname):
      uri = self.ESBASE + "/" + projectname + "/_refresh"
      logging.info("refreshing ES index %s" % projectname)
      return jsonget(uri)

      
def bp_test():
   project_name = "".join([random.choice(string.lowercase) for i in range(0,10)]) 
   logging.info("Making project %s" % project_name)
   bp = BP()
   bp.create_project(project_name)
   bugtitle = "Title fo whatever yo"
   bugdata = "Whatever yo"
   bugid = random.choice(range(0,100))
   logging.info("Adding bug %s" % bugid)      
   bp.add_bug(project_name, bugid, bugtitle, bugdata)
   logging.info("Getting bug %s" % bugid)      
   bug = bp.get_bug(project_name, bugid)
   assert bug["description"] == bugdata, "text != bugdata"
   assert str(bug["bugid"]) == str(bugid), "bugid not set %s %s %s" % (bug["bugid"], str(bugid), str(bug))
   time.sleep(1)
   logging.info("Getting similar bugs to %s" % bugid)      
   responses = bp.similar_bugs(project_name, bugdata)
   assert len(responses) == 1, "Not enough responses %s" % responses
   assert str(responses[0]["bugid"]) == str(bugid), "First response off? %s" % responses
   bp.remove_project(project_name)
   try:
      bp.add_bug(project_name, bugid, bugdata)
      assert False,"Did not fail when adding a bug to a missing project"
   except:
      pass
      
import datetime as dt

      
class BPModel(Model):
   def __init__(self,irmodel=None,bugs=None):
      super(BPModel,self).__init__(irmodel,bugs)

   def train_on_docs(self,bugs):
      logging.info("Training of bugs!")
      self.irmodel.build_corpus(bugs)
      # make bug party db
      # load
      bp = BP()
      self.bp = bp
      project_name = "cc"+dt.datetime.now().strftime("%y%m%d")+"".join([random.choice(string.lowercase) for i in range(0,10)])
      bp.create_project(project_name)
      self.project_name = project_name
      logging.info("Making a bug")
      corpus = [doc for doc in self.irmodel.get_corpus() if not doc is None]
      self.corpus = corpus
      logging.info("Loading Corpus into Bugparty")
      i = 0
      self.ids = []
      self.reverse = {}
      for doc in self.corpus:
          myid = str(i)
          if isinstance(doc, basestring):
             bp.add_bug(project_name, myid, doc[0:72], doc)
          elif isinstance(doc, list):
             mydoc = " ".join(doc)
             bp.add_bug(project_name, myid, mydoc[0:72], mydoc)
          elif isinstance(doc, dict):
             if "bugid" in doc:
                myid = str(doc["bugid"])
             bp.add_bug(project_name, myid, doc["title"], doc["description"])
          self.ids.append(myid)
          self.reverse[myid] = i
          i = i + 1
      bp.refresh(project_name)


   #def similar_from_text(self,doc):
   #   return self.similar(doc)

   #def similar_from_words(self,words):
   #   return self.similar(words)
   def get_words(self,doc):
      """ Produce a bag of words """
      words = noneTo(self.irmodel.process_document(doc))
      return words
   
   def similar(self,text):
      if isinstance(text,str):
          raise "Text was a string: "+text
      query = " ".join(text)
      response = self.bp.similar_bugs(self.project_name,query)
      # make an index of similarity per each document as a numpy array?
      similarity = np.zeros(len(self.corpus))
      i = 0
      for row in response:
         i_th = i + 1
         score = 1.0/i_th
         index = self.reverse[str(row["bugid"])]
         similarity[index] = score
         i = i_th
      return similarity
   
   def clean(self):
      logging.info("Removing Bugparty Project %s" % self.project_name)
      self.bp.remove_project(self.project_name)




      
class BM25Model(Model):
   def __init__(self,irmodel=None,bugs=None):
      super(BM25Model,self).__init__(irmodel,bugs)

   def make_index(self, corpus):
      self.bm25 = bm25.BM25(corpus)
      # https://github.com/RaRe-Technologies/gensim/blob/develop/gensim/summarization/bm25.py
      self.avg_idf = sum(float(val) for val in self.bm25.idf.values()) / len(self.bm25.idf)
      return self.bm25
      
   def train_on_docs(self,bugs):
      self.avg_idf = 0.0
      logging.info("Training of bugs! [bm25]")
      self.irmodel.build_corpus(bugs)
      words = self.irmodel.get_words()
      logging.info("irmodel get words %s" % len(words))
      dictionary = gensim.corpora.Dictionary([words])
      self.dictionary = dictionary
      #corpus = [doc for doc in self.irmodel.get_corpus() if not doc is None]
      corpus = [dictionary.doc2bow(noneTo(doc)) for doc in self.irmodel.get_corpus() if doc is not None]
      logging.info("Corpus Length %s" % len(corpus))
      self.corpus = corpus

      logging.info("Making a BM25 Corpus")
      self.index = self.make_index(corpus)
      # self.bm25 = altscheme.JustIDF(corpus)
      #self.bm25 = altbm25.BM25(corpus)
      #self.avg_idf = np.average(self.bm25.idf.values())
      
   def similar(self,words):
      if isinstance(words,str):
          raise "words was a string: "+words
      response = self.index.get_scores(words,self.avg_idf)
      #response = self.bm25._get_scores(words)
      # should be a list with similar of 1 being good?
      return np.array(response)

   def clean(self):
      pass

class IDFModel(BM25Model):
   def __init__(self,irmodel=None,bugs=None):
      super(IDFModel,self).__init__(irmodel,bugs)
   def make_index(self, corpus):
      self.justidf = altscheme.JustIDF(corpus)
      return self.justidf

class TFIDFModel(BM25Model):
   def __init__(self,irmodel=None,bugs=None):
      super(TFIDFModel,self).__init__(irmodel,bugs)
   def make_index(self, corpus):
      logging.info("Making a TFIDF Model")
      tfidf = gensim.models.tfidfmodel.TfidfModel(corpus)
      self.tfidf = tfidf
      logging.info("Making a TFIDF Corpus")
      tfidfcorp = [tfidf[doc] for doc in corpus]
      self.tfidfcorp = tfidfcorp
      logging.info("Making a similarity index")
      dictionary = self.dictionary
      index = gensim.similarities.docsim.MatrixSimilarity(tfidfcorp, num_features=dictionary.num_pos)
      self.index = index
      # https://github.com/RaRe-Technologies/gensim/blob/develop/gensim/summarization/bm25.py
      return self.index
   
   def similar(self,words):
      tfidfdoc = self.tfidf[words]
      distances = self.index[tfidfdoc]
      return distances


   
def test_BM25():
   my_bugs = bug.load_bugs("data/small_eclipse_issues.json")
   logging.info("Make Bug Context")
   bugcontext = BM25BugContext(my_bugs)
   logging.info("Make IR")      
   irexp = make_ir({})
   logging.info("Make Model")      
   model = bugcontext.make_model(irmodel=irexp,bugs=my_bugs)
   cq = ContQuery(bugcontext, model, 25)
   words = model.get_words_seq(my_bugs[5].get_title())
   queries = cq.get_queries(words,25)
   dupes_id = bugcontext.dupes_i_of(5)
   ranked_queries = [rank_query(query, dupes_id) for query in queries]
   for ranked_query in ranked_queries:
      subset = set([ranked_query[1][0], ranked_query[1][1]])
      assert(5 in subset)
      assert(6 in subset)
   list_of_bools = [x[2] for x in ranked_queries]
   res = cq.continuous_query(my_bugs[5], dupes_id)
   print(res)
   assert(res[0]["TOP5"] == 1.0)
   assert(res[0]["MRR"] >= 0.5)
   
   
def rank_query(query,dupe_ids):
   aids = query.argsort()[::-1]
   return (
      [query[i] for i in aids],
      [i for i in aids],
      [(i in dupe_ids) for i in aids]
   )

def reciprocal_rank(bools):
   for i in range(0,len(bools)):
      if bools[i]:
         return 1/float(i+1)
   return 0.0

def rank(bools):
   for i in range(0,len(bools)):
      if bools[i]:
         return i+1
   return len(bools)

def rank_or_none(bools):
   for i in range(0,len(bools)):
      if bools[i]:
         return i+1
   return None

def rank_or_NaN(bools):
   for i in range(0,len(bools)):
      if bools[i]:
         return i+1
   return float('nan')



def aveP(bools):
   return ireval.aveP( bools )

def MAP(list_of_bools):
   return ireval.MAP( list_of_bools )

def mrr(list_of_bools):
   ranks = [reciprocal_rank(bools) for bools in list_of_bools]
   return sum(ranks)/float(len(ranks))

def topk(list_of_bools,k=10):
   topks = [int(rank(bools) <= k) for bools in list_of_bools]
   return sum(topks)/float(len(topks))

def wtopk(list_of_bools,k=10):
   topks = [int(rank(bools) <= k)*1.0/float(i+1) for i, bools in zip(range(0,len(list_of_bools)),list_of_bools)]

   return sum(topks)/float(len(topks))

def aveP_topk(list_of_bools, k=10):
   topks = [int(rank(bools) <= k) for bools in list_of_bools]
   return aveP(topks)

def not_none_and_less_than(v,k):
   return v is not None and v <= k

def first_topk(list_of_bools, k=10):
   topks = [not_none_and_less_than(rank_or_none(bools),k) for bools in list_of_bools]
   return rank_or_NaN(topks)

def rr_topk(list_of_bools, k=10):
   topks = [not_none_and_less_than(rank_or_none(bools),k) for bools in list_of_bools]
   return mrr([topks])


def averages(res):
   if len(res) > 0:
      avgs = {key: 0 for key in res[0]}
      for key in res[0].keys():
         avgs[key] = np.nanmean([r.get(key,0) for r in res])
   else:
      avgs = dict()   
   return avgs


class DeDupQuery(object):

   def run_dedup(self,model,bugs,bugcontext):
      logging.info("Calculating MAP and Whatnot")
      aveps = list()
      ids = bugcontext.ids
      dup_ids = bugcontext.dup_ids
      for i in xrange(0,len(ids)):
         if not bugcontext.is_dupe(ids[i]):
            continue
         #logging.info("i:%s"%i)
         distances = model.similar(model.tfidfcorp[i])
         actual_id = ids[i]
         dupe_id = dup_ids[i]
         if dupe_id == None or dupe_id == '':
            dupe_id = actual_id
         #distances = shd[i]
         # grr, reverse the results and ignore the first because it should be the same
         # document
         aids = distances.argsort()[::-1]
         aids = aids[aids!=i]
         result = dup_ids[aids].astype(object) == dupe_id
         averageP = ireval.aveP( result )
         aveps.append(averageP)
         #print averageP

      myMAP = sum(aveps)/len(aveps)
      tfidfmap = myMAP
      print "TFIDF MAP %s" % myMAP
      logging.info("TFIDF MAP %s" % myMAP)
      pickle.dump(dict(aveps=aveps, tfidfmap=tfidfmap),file("tfidf.out",'w'))

   
class ContQuery(object):
   def __init__(self,bugcontext,model, max_words):
      self.model = model
      self.bc = bugcontext
      self.max_words = max_words

   def get_queries(self, words, max_words):
      model = self.model
      # should range be range(1,max_words) 
      # >>> [1][0:0]
      # []
      # >>> [1][0:1]
      # [1]
      queries = (model.similar_from_words(words[0:i]) for i in range(1,max_words+1))
      return queries
   
   def continuous_query(self,input_text, dupes_id):
      model = self.model
      words = model.get_raw_words_seq(input_text)
      try:
          # potential bug with splitting and stop words being
          # inconsistent
          queries = self.get_queries(words,self.max_words)
      except urllib2.HTTPError as e:
          logging.info("#####################")
          logging.info("Query failure")
          logging.info(str(e))
          logging.info("Words:" + str(words))
          logging.info("#####################")
          queries = [np.array([])]
          # I think there's a bug here and it should return 0s and ranked_queries
          return {
             "MRR": 0.0,
             "MAP": 0.0,
             "TOP5": 0.0,
             "TOP1": 0.0,
             "TOP10": 0.0,
             "aveP-TOP5": 0.0,
             "first-TOP5": 0.0
          }, list()
      # queries = (model.similar_from_words(words[0:i]) for i in range(0,max_words))
      # now we have to mark dupes
      # converting distance vector into a list of ranks of dupes
      ranked_queries = [rank_query(query, dupes_id) for query in queries]
      list_of_rank1 = [x[1][0] for x in ranked_queries]
      # logging.info('Rank1: %s' % list_of_rank1)
      logging.info('Rank1Set: %s' % len(set(list_of_rank1)))
      list_of_bools = [x[2] for x in ranked_queries]
      our_mrr = mrr(list_of_bools)
      my_map  = MAP(list_of_bools)
      our_top5 = topk(list_of_bools,k=5)
      our_top1 = topk(list_of_bools,k=1)
      our_top10 = topk(list_of_bools,k=10)
      avgptop5 = aveP_topk(list_of_bools,k=5)
      first_top5 = rr_topk(list_of_bools, k=5)
      return {
         "MRR": our_mrr,
         "MAP": my_map,
         "TOP5": our_top5,
         "TOP1": our_top1,
         "TOP10": our_top10,
         "aveP-TOP5": avgptop5,
         "first-TOP5": first_top5
      }, ranked_queries

      
   def self_query(self):
      res = list()
      max_words = self.max_words
      model = self.model
      bugcontext = self.bc
      ids = bugcontext.ids
      my_bugs = bugcontext.bugs
      for i in xrange(0,len(ids)):
         if not bugcontext.is_dupe(ids[i]):
            continue
         dupes = bugcontext.dupes_i_of(i)
         # changed here
         lres, _ = self.continuous_query(my_bugs[i].desc(), dupes)
         print "%s %s %s" % (i,ids[i],lres)
         res.append(lres)
      
      avgs = {key: 0 for key in res[0]}
      for key in res[0].keys():
         avgs[key] = np.nanmean([r[key] for r in res])
      
      print("AVGS %s" % avgs)
      pickle.dump(dict(res=res, avgs=avgs),file("avgs.out",'w'))

   def query(self,test):
      res = list()
      max_words = self.max_words
      model = self.model
      bugcontext = self.bc
      test_bc = BugContext(test)
      test_bugs = test_bc.bugs
      for bug in test_bc.bugs:
         # are there dupes in the corpus?
         if not bugcontext.is_dupe(bug.get_id()):
            continue
         dupes = bugcontext.dupes_of_bug(bug)
         # lookout = set([31,44,125,407,297,405])
         # # logging.info("Dupes %s" % dupes)
         # if int(bug.get_id()) in lookout:
         #    import pdb
         #    pdb.set_trace()
         # changed here
         lres, _ = self.continuous_query(bug.desc(), dupes)
         logging.info("%s %s" % (bug.get_id(),lres))
         res.append(lres)
      if len(res) > 0:
         avgs = {key: 0 for key in res[0]}
         for key in res[0].keys():
            avgs[key] = np.nanmean([r[key] for r in res])
      else:
         avgs = dict()   
      print("AVGS %s" % avgs)
      out = dict(res=res, avgs=avgs)
      return out

class NotSoContQuery(ContQuery):
   """This is not a continuous query, it is a entire query query"""
   def get_queries(self, words, max_words):
      model = self.model
      self.max_words = max_words
      queries = [model.similar_from_words(words)]
      return queries

class NWordsContQuery(ContQuery):
   """This is not a continuous query, it is a entire query query"""
   def get_queries(self, words, max_words):
      model = self.model
      self.max_words = max_words
      queries = [model.similar_from_words(words[0:(self.max_words+1)])]
      return queries

   
   
def id_splitter(bugcontext,partitions=10):
   """ Takes bugs and splits them across by ID into 10 train/test groups """   
   # a|b|c|d|e| you have to divide by partitions + 1
   # e.g partitions = 4, N = 5, N/(partitions+1)=1,2,3,4
   # assume sorted?
   skip = int(ceil(bugcontext.len() / float(partitions+1)))
   bugs = bugcontext.bugs
   train_test = [(bugs[0:i+skip],bugs[i+skip:]) for i in range(0,len(bugs),skip)][0:partitions]
   return train_test

def cross_folds(bugcontext,folds=10):
   bugs = list(bugcontext.bugs)
   skip = int(ceil(bugcontext.len() / float(partitions)))
   random.shuffle(bugs)
   train_test = [(bugs[0:i] + bugs[i+skip:],bugs[i:i+skip]) for i in range(0,len(bugs),skip)]
   return train_test
   

def run_exp(train,test,irexp,bugcontext,max_words,notcont=False,nwords=False):
   logging.info("Run EXP")
   newbc = bugcontext.make_instance(train)
   logging.info("Make IR %s" % newbc.len())      
   irexp = irexp.make() # make a new version w/ empty corpus
   logging.info("Make Model")
   my_bugs = newbc.bugs
   model = newbc.make_model(irmodel=irexp,bugs=my_bugs)
   if notcont:
      logging.info("Making a NOT-Continuous Query")      
      cq = NotSoContQuery(newbc, model, max_words)
   elif nwords:
      logging.info("Making a NWords-Continuous Query")      
      cq = NWordsContQuery(newbc, model, max_words)
   else:
      cq = ContQuery(newbc, model, max_words)
   out = cq.query(test)
   model.clean()
   return out

def test_run_exp_with_bp(args):
   max_words = int(args.words)
   my_bugs = load(args)
   logging.info("Make Bug Context")
   bugcontext = BPBugContext(my_bugs)
   logging.info("Make IR")      
   irexp = make_ir(args)
   logging.info("SPlitting")      
   train_tests = id_splitter(bugcontext,int(args.parts))
   logging.info("Run EXPS")      
   output = run_exp(train_tests[0][0],train_tests[0][1],irexp,bugcontext,max_words,args.notcont)
   print(output)
   pickle.dump(output,file("test_run_exp_with_bp.pkl",'w'))
   
   
   
              
def run_exps(train_tests,irexp,bugcontext,max_words,notcont=False,nwords=False):
   outs = list()
   count = 1
   for (train,test) in train_tests:
      #if count in set([12,53,62,69,76]):
      #if count in set([69,76]):
      #if count in set([12,53,62]):
      #   import pdb
      #   pdb.set_trace()
      logging.info("Running Exp %s/%s" % (len(train),len(test)))
      res = run_exp(train,test,irexp,bugcontext,max_words,notcont,nwords)
      print res["avgs"]
      outs.append(res)
      count += 1
   return outs

def load(args):
      if not os.path.isfile(args.bp):
         logging.info("loading bugs from %s" % args.i)
         my_bugs = bug.load_bugs(args.i)
         # we don't need to duplicate disk space 
         # logging.info("saving out to %s" % args.bp)
         # pickle.dump(my_bugs,file(args.bp,'w'))
      else:
         logging.info("Loading Pickle of Bugs")
         my_bugs = pickle.load(file(args.bp))
      return my_bugs

def make_ir(args):
   if getattr(args,"nostopwords",False):
      stopworder = IRExp.WordFilter()
   else:
      logging.info("Filtering Stop Words")
      stopworder = IRExp.StopWordFilter()
   if getattr(args,"nostem",False):
      stemmer = IRExp.Stemmer()
   else:
      stemmer = IRExp.PorterStemmer()
   if getattr(args,"nopunct",False):
      char_filter = IRExp.StripNonAlpha()
   else:
      char_filter = IRExp.AllCharCharFilter()
   if getattr(args,"keepdigits",False):      
      digit_filter = IRExp.AllCharCharFilter()
   else:
      digit_filter = IRExp.StripDigits()
   if getattr(args,"nolowercase",False):
      caser = IRExp.Caser()
   else:
      caser = IRExp.LowerCaser()
   return IRExp.IRExp({
      "word_filter":stopworder,
      "stemmer":stemmer,
      "char_filter":char_filter,
      "digit_filter":digit_filter,
      "caser":caser
   })

   
def cmd_cont_query(args):
   logging.info("Running Cont Query")      
   max_words = int(args.words)
   my_bugs = load(args)
   logging.info("Make Bug Context")
   if args.bugparty:
      bugcontext = BPBugContext(my_bugs)
   elif args.bm25:
      bugcontext = BM25BugContext(my_bugs)
   elif args.justidf:
      bugcontext = IDFBugContext(my_bugs)
   elif args.justtfidf:
      bugcontext = TFIDFBugContext(my_bugs)
   else:
      bugcontext = BugContext(my_bugs)
   logging.info("Make IR")      
   irexp = make_ir(args)
   logging.info("SPlitting")      
   train_tests = id_splitter(bugcontext,int(args.parts))
   logging.info("Run EXPS")      
   output = run_exps(train_tests,irexp,bugcontext,max_words,args.notcont,args.nwords)
   avgs = [x["avgs"] for x in output]
   avgavg = averages(avgs)
   pickle.dump({"output":output,"avgavg":avgavg},file(args.contquerypkl,'w'))   


   
def cmd_main(args):
   logging.info("Running Main")
   max_words = int(args.words)
   my_bugs = load(args)
   logging.info("Make Bug Context")
   bugcontext = BugContext(my_bugs)
   logging.info("Make IR")      
   irexp = make_ir(args)
   logging.info("Make Model")      
   model = Model(irmodel=irexp,bugs=my_bugs)   
   logging.info("DeDupQuery")
   dedupquery = DeDupQuery()
   dedupquery.run_dedup(model, my_bugs, bugcontext)
   logging.info("Continous Query")      
   cq = ContQuery(bugcontext, model, max_words)
   cq.self_query()

def run_tests(args):
   test_BM25()
   bp_test()
   test_run_exp_with_bp(args)

if __name__ == "__main__":
   args = parse_args()
   init_basic_logging(args.log)
   if args.test:
      run_tests(args)
   elif args.runcont:
      cmd_cont_query(args)
   else:
      cmd_main(args)
