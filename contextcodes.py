""" Use our secontext on text """
import climate
import math
import numpy
import numpy as np
import nltk
import matplotlib.pyplot as plt
import sys
import logging
from nltk.tokenize import regexp_tokenize, wordpunct_tokenize
logging.basicConfig(stream = sys.stderr, level=logging.INFO)
import matplotlib.pyplot as plt
import scipy
import scipy.spatial
import scipy.spatial.distance
#import mutators
import cPickle
import cPickle as pickle

contexts = ["NFR-efficiency"
            ,"NFR-functionality"
            ,"NFR-maintainability"
            ,"NFR-portability"
            ,"NFR-reliability"
            ,"NFR-usability"
            ,"pressman-Architecture"
            ,"pressman-Design"
            ,"pressman-FormalMethods"
            ,"pressman-Managing"
            ,"pressman-Metrics"
            ,"pressman-modeling"
            ,"pressman-OOPs"
            ,"pressman-Process"
            ,"pressman-Reengeering"
            ,"pressman-Testing"
            ,"pressman-UIDesign"
            ,"pressman-Webengineering"
            ,"SWEBOK-comp"
            ,"SWEBOK-economics"
            ,"SWEBOK-practice"
            ,"SWEBOK-SEmanage"
            ,"SWEBOK-configuration"
            ,"SWEBOK-eng"
            ,"SWEBOK-process"
            ,"SWEBOK-SEmethods"
            ,"SWEBOK-construction"
            ,"SWEBOK-maintain"
            ,"SWEBOK-quality"
            ,"SWEBOK-testing"
            ,"SWEBOK-design"
            ,"SWEBOK-maths"
            ,"SWEBOK-requirements"
]

context_words = dict()

def get_context_words():
    global context_words
    return context_words

def load_contexts(context_words):
    for context in contexts:
        path = "contexts/%s" % context
        s = set(word.strip() for word in file(path).readlines())
        if '' in s:
            s.remove('')
        context_words[context] = s                

load_contexts(context_words)

def init_basic_logging():
    logging.basicConfig(stream = sys.stderr, level=logging.INFO)

logger = logging.getLogger('contextcodes')

standardization = False

def set_standardization(val):
    global standardization
    standardization = val
    return standardization

def contexts_of_words(words):
    lwords = [word.lower() for word in words]    
    context_intersect = dict()
    for i in xrange( 0, len(contexts) ):
        context = context_words[ contexts[i] ]
        context_intersect[ contexts[i] ] = context_intersect.get(contexts[i],0) + len( context.intersection( lwords ) )
    return context_intersect

def context_token(context):
    return "__CONTEXT:%s__" % context

def add_contextwords_to_words(words):
    context_counts = contexts_of_words(words)
    for context in context_counts:
        token = context_token(context)
        for i in range(0,context_counts[context]):
            words.append(token)
    return words

def sentence_to_input(sentence):
    ''' a hashed word count '''
    global ninputs
    global binputs
    global context_words
    global contexts
    words = wordpunct_tokenize(sentence)
    # hashes = [djb.djb2cached(word) for word in words]
    hashes = words
    out = np.zeros(ninputs)
    for i in xrange(0,len(hashes)):
        out[hashes[i]] += 1
    # now add context
    for i in xrange(0,len(contexts)):
        context = context_words[contexts[i]]
        out[binputs + i] = len(context.intersection(words))
    if standardization:
        return out.astype(numpy.float32)/(out+1)
    else:
        return out

def doc2words(doc):
    if (doc == None):
        return []
    return wordpunct_tokenize(doc)

