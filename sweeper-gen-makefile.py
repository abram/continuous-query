""" Generate a makefile for a parameter sweep """
import projects
import jinja2
projs = projects.Projects().projects
output = "exp3"
makefile_template_name = "./sweep-makefile.template"
template = jinja2.Template(file(makefile_template_name).read())
counts=[1,5,10,15,20,25,50,100,200,400,800,1600,3200]
jsonfiles = [x + "_issues.json" for x in projs]
combos = [{"count":count,"project":proj} for count in counts for proj in jsonfiles]

print(template.render(jsonfiles=jsonfiles,
                      combos=combos,
                      output=output
))

