mkdir exp1
JSONS="eclipse_issues.json openoffice_issues.json mozilla_issues.json android_issues.json tempest_issues.json"
#JSONS="eclipse_issues.json openoffice_issues.json mozilla_issues.json"
for file in $JSONS
do
	DIR=exp1/$file
	echo mkdir $DIR
	mkdir $DIR
	echo python -i contquery.py \
		-i data/$file \
		--bp $DIR/bp.pkl \
		--runcont \
		--parts 100 \
		--log $DIR/contquery.log \
		--contquerypkl $DIR/contquery.pkl &
done
