# select issues
# select dupes
# select comments
import storm
from storm.locals import *
import json
# oh look it requires bicho!
import bicho.db.database
from bicho.db.database import DBIssue, DBComment, DBIssueRelationship
import argparse

def parse_args(add_args=None):
    parser = argparse.ArgumentParser(description='bicho 2 json')
    parser.add_argument('--out', default="bicho.json", help='write output to file')
    parser.add_argument('--user', default="bicho", help='user')
    parser.add_argument('--host', default="localhost", help='host of user')
    parser.add_argument('--password', default='bicho', help="password")
    parser.add_argument('--project', default='bzr', help="project")
    parser.set_defaults()
    if (add_args!=None):
        add_args(parser)
    args = parser.parse_args()
    return args

args = parse_args()

database =create_database("mysql://%s:%s@%s/%s" % (args.user,args.password,args.host,args.project))
store = Store(database)

# Get all Issues
result = store.find(bicho.db.database.DBIssue)

def mk_comment(comment):
    return { "what":comment.text,"when":str(comment.submitted_on),"who":comment.submitted.name }

def mk_issue(issue, comments, related_tos):
    mergeID = ""
    if related_tos.count() > 0:
        mergeID = related_tos[0].issue.issue
    return {
        "doc": { "_id": issue.issue,
                 "bugid": issue.issue,
                 "author": issue.submitted.name,
                 "title": issue.summary,
                 "description": issue.description,
                 "published": str(issue.submitted_on),
                 "comment":[mk_comment(comment) for comment in comments],
                 "mergeID":mergeID
             }
    }

fd = file(args.out,"w")

def write_out(str):
    print str
    if fd != None:
        fd.writelines([str])

write_out('{ "rows" : [ ')
first = True
for issue in result:    
    if not first:
        write_out(",")
    else:
        first = False
    issue_id = issue.id
    db_comments = store.find(DBComment, DBComment.issue_id == issue_id)
    related_tos = store.find(DBIssueRelationship, DBIssueRelationship.related_to == issue_id,DBIssueRelationship.type.like(u"duplicate_of"))
    obj = mk_issue(issue, db_comments, related_tos)
    write_out(json.dumps(obj,indent=2))


write_out('] }')

fd.close()
