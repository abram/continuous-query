try:
   import cPickle as pickle
except:
   import pickle
import argparse
import os
import os.path
import sys
import logging
import numpy as np

def init_basic_logging(logfile="summarize-parameter-sweep.log"):
    logging.basicConfig(filename=logfile, stream = sys.stderr, level=logging.INFO)
    # stderr out
    logging.getLogger().addHandler(logging.StreamHandler())

def parse_args(add_args=None):
    parser = argparse.ArgumentParser(description='Extract stats about maxwords')
    parser.add_argument('--prefix',default="",help="Prefix")
    parser.add_argument('files', help='Filenames',nargs='*')
    if (add_args!=None):
        add_args(parser)
    args = parser.parse_args()
    return args

init_basic_logging()
 
ourkeys = ['MAP', 'MRR', 'TOP1', 'TOP5', 'TOP10', 'aveP-TOP5']

args = parse_args()
prefix = args.prefix

if len(args.files) == 0:
   import projects
   projs = projects.Projects().projects
   counts=[1,5,10,15,20,25,50,100,200,400,800,1600,3200]
   # default behaviour
   files = ["exp3/" + proj + "_issues.json/contquery.words." + str(count) + ".pkl" for proj in projs for count in counts]
else:
   files = args.files


def _averages(res,columns):
   if len(res) > 0:
      avgs = {key: 0 for key in columns}
      for key in columns:
         avgs[key] = np.average([r.get(key,0) for r in res])
   else:
      avgs = dict()   
   return avgs


def averages(dicts,columns):
   nonempty = [y['avgs'] for y in dicts if len(y['avgs']) > 0]
   return _averages(nonempty, columns)

def mymain(args):
   out = []
   out += [ ",".join(["project","words"] + ourkeys) ]   
   for file_name in files:
      try:
          res = pickle.load(file(file_name))
      except IOError:
          continue
      # avgs = res["avgavg"]
      # avgs = res["avgavg"]
      avgs = averages(res["output"],ourkeys)
      base_filename = file_name.split("/")[-1]
      base_dir = file_name.split("/")[-2]   
      parts = base_filename.split(".")
      project = parts[0]
      max_words = parts[2]
      pstr = ",".join([base_dir,max_words] + [str(avgs.get(k,'')) for k in ourkeys])
      out += [ pstr ]

   print("\n".join(out))
      
if __name__ == "__main__":
   mymain(args)
   
   
