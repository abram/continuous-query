import string
import re
import nltk
from nltk.tokenize import regexp_tokenize, wordpunct_tokenize

def none_dfl(value,dfl):
    if (value==None):
        return dfl
    return value


class WordFilter(object):
    def filter_words(self,words):
        return words

class StopWordFilter(WordFilter):
    def __init__(self,filename=None,values=None):
        """values is a set"""
        self.values = none_dfl(values,list())
        self.filename = filename
        if (self.filename == None):
            self.values = set(open("stop_words").read().splitlines())
        self.values.add("")
    
    def filter_words(self, tokens):
        return [x for x in tokens if not(x in self.values)] 


class IdentityCharFilter(object):
    def filter(self,value):
        return value

AllCharCharFilter = IdentityCharFilter

class StripDigits(object):
    def filter(self, value):
        return re.sub(r'[0-9]+','',value)

class StripNonAlpha(object):
    def filter(self, value):
        return re.sub(r'[^a-zA-Z \r\n]+','',value)


class Stemmer(object):
    def stem(self,words):
        return words

class PorterStemmer(Stemmer):
    def __init__(self):
        self.stemmer = nltk.stem.porter.PorterStemmer()
    def stem(self,words):
        return [self.stemmer.stem(word) for word in words]

class Caser(object):
    def case(self,words):
        return words

class LowerCaser(Caser):
    def case(self,words):
        return [word.lower() for word in words]

class WordSplitter(object):
    def split(self,str_of_words):
        return re.split(r'[^\w]+',str_of_words)

class WordPunctSplitter(object):
    def split(self,str_of_words):
        return wordpunct_tokenize( str_of_words )
    


class IRExp(object):
    def __init__(self,dargs=None):
        """Template Method Pattern Class to define an IR experiment"""
        self.word_filter = dargs.get("word_filter", WordFilter())
        self.char_filter = dargs.get("char_filter", AllCharCharFilter()) 
        self.digit_filter = dargs.get("digit_filter", AllCharCharFilter())
        self.word_splitter = dargs.get("tokenizer",WordSplitter())
        self.caser = dargs.get("caser",Caser())
        self.stemmer = dargs.get("stemmer",Stemmer())
        self.distance = dargs.get("distance","cosine")
        self.documents = list()
        self.corpus = list()
    def make(self):
        return IRExp(dargs={
            "word_filter":self.word_filter,
            "char_filter":self.char_filter,
            "digit_filter":self.digit_filter,
            "tokenizer":self.word_splitter,
            "caser":self.caser,
            "stemmer":self.stemmer,
            "distance":self.distance
        })
    def process_document(self, document):
        # is there a bug here with document.text ?
        text = document if isinstance(document,str) or isinstance(document,unicode) else document.desc()
        cf = self.char_filter.filter( text )
        df = self.digit_filter.filter( cf )
        words = self.word_splitter.split( df )
        cased = self.caser.case( words )
        words = self.word_filter.filter_words( cased )
        stemmed = self.stemmer.stem( words )
        emptyless = self.filter_empties( stemmed )
        return emptyless

    def filter_empties(self, words):
        return [x for x in words if not x == '']
    
    def build_corpus(self,documents):
        self.documents = documents
        self.corpus = [self.process_document(document) for document in documents]
        return self.corpus

    def add_document(self, document):
        self.corpus.append(self.process_document(document))    
        self.documents.append(document)    

        
    def add_documents(self, documents):
        for document in documents:
            self.add_document(document)

        
    def get_corpus(self):
        return self.corpus

    def get_words(self):
        out = set()
        for words in self.corpus:
            out.update( words )
        return out
    
    def documents(self):
        return self.documents

def make_a_good_ir(stemmer=None):    
    return IRExp({
        "word_filter":StopWordFilter(),
        "stemmer":none_dfl(stemmer,PorterStemmer()),
        "digit_filter":StripDigits(),
        "caser":LowerCaser()
    })
