import logging
    
def topk( boolranked, k=10):
    total = 0
    for x in boolranked:
        if x:
            total += 1
    return total / float(k)

def top1( ranks ):
	return topk(ranks, k=1)
def top5( ranks ):
	return topk(ranks, k=5)
def top10(bugs, query):
	return topk(ranks, k=10)

def aveP( ranks, rel=0 ):
    seen = 0
    mysum = 0
    lastseen = None
    for i in xrange(0, len(ranks)):
        if ranks[i]:
            seen += 1
            mysum += seen / float(i + 1)
            lastseen = i
    if seen == 0:
        return 0
    else:
        myap = mysum / float(max(seen,rel))
        return myap

def MAP(ranks_of_ranks):
    avePs = [aveP(rank) for rank in ranks_of_ranks]
    return sum(avePs)/len(avePs)

def calc_MAP(aveps):
    if len(aveps) == 0:
        logging.info("0 averagePrecisions")
        return 0
    return sum(aveps)/float(len(aveps))
