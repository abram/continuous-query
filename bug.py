import json

def nE(x):
    if x == None:
        return ""
    if x == {}:
        return ""
    return x

def dict2l(c):
   if type(c) == type({}):
      return [c]
   else:
      return c

class Bug(object):
    def __init__(self,guts=None):
        if guts != None:
            for key in guts.keys():
                if key == "dup_id":
                    setattr(self,"mergeID",guts[key])
                else:
                    setattr(self,key,guts[key])
                    if key == "_id":
                        self.bugid = guts[key]

    def get_id(self):
        return getattr(self, "_id", getattr(self,"bugid",None))

    def dup_id(self):
        return getattr(self,"mergeID",None)
   
    def all_text(self):
        bug = self
        try:
            txt = "\n".join([nE(getattr(bug,"title","")),nE(getattr(bug,"description",""))])
            comments = "\n".join([nE(comment.get("what","")) for comment in dict2l(getattr(bug,"comment",[]))])
            return "\n".join([txt,comments])
        except Exception as e:
            print bug
            raise e

    def get_title(self):
        return nE(getattr(self,"title",""))

    def get_description(self):
        return nE(getattr(self,"description",""))
    def get_published(self):
        return getattr(self,"published",
                    getattr(self,"openedDate",None))

    def just_title_desc(self):
        bug = self
        try:
            txt = "\n".join([nE(getattr(bug,"title",""))])
            return txt
        except Exception as e:
            print bug
            raise e
    
    def desc(self):
        bug = self
        try:
            txt = "\n".join([nE(getattr(bug,"title","")),nE(getattr(bug,"description",""))])
            #txt = "\n".join([nE(getattr(bug,"title",""))])
            return txt
        except Exception as e:
            print bug
            raise e
      

def load_bugs(filename):
   v = json.load(file(filename))
   if type(v) == type({}):
      bugs = v["rows"]
      bugs = [bug["doc"] for bug in bugs]
   elif type(v) == type([]):
      bugs = v
   else:
      raise Exception("Can't load")
   try:
      bugs = sorted(bugs,key=lambda x: int(x["_id"]))
   except ValueError:
      # just sort as string
      logging.warn("Couldn't convert IDs to integers, now sorting as string")
      bugs = sorted(bugs,key=lambda x: str(x["_id"]))
   my_bugs = [Bug(bug) for bug in bugs]
   return my_bugs
