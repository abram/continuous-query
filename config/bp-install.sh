export LC_ALL="en_US.UTF-8"
export LC_CTYPE="en_US.UTF-8"
sudo apt-get -y install docker.io
sudo usermod -a -G docker ubuntu
sudo service docker restart
cd ~
mkdir projects
cd projects
mkdir bugparty
cd bugparty
git clone https://bitbucket.org/abram/bugparty.git -b mvel
git clone https://bitbucket.org/abram/bugparty-docker
cd bugparty-docker
./devenv build
