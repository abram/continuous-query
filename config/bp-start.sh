export LC_ALL="en_US.UTF-8"
export LC_CTYPE="en_US.UTF-8"
cd ~
cd projects
cd bugparty
cd bugparty-docker
git pull origin master
./devenv up
sleep 30
./devenv headless
