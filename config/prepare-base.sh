export LC_ALL="en_US.UTF-8"
export LC_CTYPE="en_US.UTF-8"
sudo locale-gen
sudo apt-get update && sudo apt-get -y upgrade
sudo apt-get install -y python python-pip
echo 'export PYTHONPATH="${HOME}/.local/lib/python2.7/site-packages/:$PYTHONPATH"' >> ~/.bashrc
echo $(hostname -I | cut -d\  -f1) $(hostname) | sudo tee -a /etc/hosts
