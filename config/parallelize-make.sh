find projects -iname '*.pkl' | sed -e 's,projects/continuous-query/,,' > .existingpkl
bash targets.sh  | fgrep -vf .existingpkl | \
    parallel \
             --verbose \
             --retries 3 \
             --sshloginfile config/slaves  \
             --return ./projects/continuous-query/{} \
             'cd projects/continuous-query; make {}' 
