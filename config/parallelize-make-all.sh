find projects -iname '*.pkl' | sed -e 's,projects/continuous-query/,,' > .existingpkl.all
bash alltargets.sh  | fgrep -vf .existingpkl.all | \
    parallel \
             --verbose \
             --retries 3 \
             --sshloginfile config/slaves  \
             --return ./projects/continuous-query/{} \
             'cd projects/continuous-query; make -f Makefile.everything {}' 
