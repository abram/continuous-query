export LC_ALL="en_US.UTF-8"
export LC_CTYPE="en_US.UTF-8"
sudo locale-gen
sudo apt-get install -y python python-pip unzip
sudo -u root pip install storm gensim jinja2 numpy scipy matplotlib nltk
cd ~
mkdir projects
cd projects
git clone -b master https://abram@bitbucket.org/abram/continuous-query.git
cd continuous-query
wget https://archive.org/download/2016-04-09ContinuousQueryData/continuous-query-data.2018-03-02.tar.gz
tar zxvf continuous-query-data.2018-03-02.tar.gz
