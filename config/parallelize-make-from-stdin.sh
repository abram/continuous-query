parallel \
         --verbose \
         --retries 3 \
         --sshloginfile config/slaves  \
         --return ./projects/continuous-query/{} \
         'cd projects/continuous-query; make -f Makefile.everything {} '
