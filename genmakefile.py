import projects
targets = list()
nottargets = list()
bptargets = list()
notbptargets = list()
bm25targets = list()
notbm25targets = list()
titletargets = list()

print "\nexp1:"
print "\tmkdir exp1\n"
print "\nexptitle:"
print "\tmkdir exptitle\n"
print "\nexpnostemtitle:"
print "\tmkdir expnostemtitle\n"
#projects = ["openoffice","tempest","eclipse","mozilla","android","openstack","bzr","k9mail"]
#projects = [ "androidbluezime", "android", "androidwifitether", "ankidroid", "appinventorforandroid", "bzr", "chrometophone", "cyanogenmod", "eclipse", "k9mail", "mozilla", "mytracks", "openoffice", "openstack", "osmand", "tempest" ]
projs = projects.Projects().projects
for proj in projs:
        target = "exp1/%s_issues.json/contquery.pkl" % proj
        targets.append(target)
        print "%s: exp1" % target
        print "\tmkdir exp1/%s_issues.json||echo done" % proj
        print "\tpython contquery.py -i data/%s_issues.json --bp exp1/%s_issues.json/bp.pkl --runcont --parts 100 --log exp1/%s_issues.json/contquery.log --contquerypkl exp1/%s_issues.json/contquery.pkl" % (proj, proj, proj, proj)

        # BP 
        target = "exp1/%s_issues.json/contquery.bugparty.pkl" % proj
        targets.append(target)
        bptargets.append(target)
        print "%s: exp1" % target
        print "\tmkdir exp1/%s_issues.json||echo done" % proj
        print "\tpython contquery.py --bugparty -i data/%s_issues.json --bp exp1/%s_issues.json/bp.pkl --runcont --parts 100 --log exp1/%s_issues.json/contquery.bugparty.log --contquerypkl exp1/%s_issues.json/contquery.bugparty.pkl" % (proj, proj, proj, proj)

        # BM25
        target = "exp1/%s_issues.json/contquery.bm25.pkl" % proj
        targets.append(target)
        bm25targets.append(target)
        print "%s: exp1" % target
        print "\tmkdir exp1/%s_issues.json||echo done" % proj
        print "\tpython contquery.py --bm25 -i data/%s_issues.json --bp exp1/%s_issues.json/bp.pkl --runcont --parts 100 --log exp1/%s_issues.json/contquery.bm25.log --contquerypkl exp1/%s_issues.json/contquery.bm25.pkl" % (proj, proj, proj, proj)

        
        # notcont
        target = "exp1/%s_issues.json/notcontquery.pkl" % proj
        targets.append(target)
        nottargets.append(target)
        print "\n%s: exp1" % target
        print "\tmkdir exp1/%s_issues.json||echo done" % proj
        print "\tpython contquery.py -i data/%s_issues.json --bp exp1/%s_issues.json/nbp.pkl --runcont --parts 100 --log exp1/%s_issues.json/notcontquery.log --contquerypkl exp1/%s_issues.json/notcontquery.pkl --notcont" % (proj, proj, proj, proj)

        # nwords (25)
        target = "exp1/%s_issues.json/nwordscontquery.pkl" % proj
        targets.append(target)
        nottargets.append(target)
        print "\n%s: exp1" % target
        print "\tmkdir exp1/%s_issues.json||echo done" % proj
        print "\tpython contquery.py -i data/%s_issues.json --bp exp1/%s_issues.json/nwordsbp.pkl --runcont --parts 100 --log exp1/%s_issues.json/nwordscontquery.log --contquerypkl exp1/%s_issues.json/nwordscontquery.pkl --nwords" % (proj, proj, proj, proj)

        # notcont BP
        target = "exp1/%s_issues.json/notcontquery.bugparty.pkl" % proj
        targets.append(target)
        notbptargets.append(target)
        print "\n%s: exp1" % target
        print "\tmkdir exp1/%s_issues.json||echo done" % proj
        print "\tpython contquery.py --bugparty -i data/%s_issues.json --bp exp1/%s_issues.json/nbp.pkl --runcont --parts 100 --log exp1/%s_issues.json/notcontquery.bugparty.log --contquerypkl exp1/%s_issues.json/notcontquery.bugparty.pkl --notcont" % (proj, proj, proj, proj)


        # notcont BM25
        target = "exp1/%s_issues.json/notcontquery.bm25.pkl" % proj
        targets.append(target)
        notbm25targets.append(target)
        print "\n%s: exp1" % target
        print "\tmkdir exp1/%s_issues.json||echo done" % proj
        print "\tpython contquery.py --bm25 -i data/%s_issues.json --bp exp1/%s_issues.json/nbp.pkl --runcont --parts 100 --log exp1/%s_issues.json/notcontquery.bm25.log --contquerypkl exp1/%s_issues.json/notcontquery.bm25.pkl --notcont" % (proj, proj, proj, proj)
        
        #titles
        target = "exp1/%s_issues.json/titlecontquery.pkl" % proj
        targets.append(target)
        titletargets.append(target)
        print "%s: exp1" % target
        print "\tmkdir exp1/%s_issues.json||echo done" % proj
        print "\tpython contquery.py -i data/%s_issues.json --bp exp1/%s_issues.json/bp.pkl --runcont --parts 100 --log exp1/%s_issues.json/titlecontquery.log --contquerypkl exp1/%s_issues.json/titlecontquery.pkl --justtitles" % (proj, proj, proj, proj)

        
print "\nexpnostem:"
print "\tmkdir expnostem\n"
for proj in projs:
        target = "expnostem/%s_issues.json/contquery.pkl" % proj
        targets.append(target)
        print "%s: expnostem" % target
        print "\tmkdir expnostem/%s_issues.json||echo done" % proj
        print "\tpython contquery.py -i data/%s_issues.json --bp expnostem/%s_issues.json/bp.pkl --runcont --parts 100 --log expnostem/%s_issues.json/contquery.log --contquerypkl expnostem/%s_issues.json/contquery.pkl --nostem" % (proj, proj, proj, proj)

        # BP
        target = "expnostem/%s_issues.json/contquery.bugparty.pkl" % proj
        targets.append(target)
        bptargets.append(target)
        print "%s: expnostem" % target
        print "\tmkdir expnostem/%s_issues.json||echo done" % proj
        print "\tpython contquery.py -i data/%s_issues.json --bugparty --bp expnostem/%s_issues.json/bp.pkl --runcont --parts 100 --log expnostem/%s_issues.json/contquery.bugparty.log --contquerypkl expnostem/%s_issues.json/contquery.bugparty.pkl --nostem" % (proj, proj, proj, proj)

        # BM25
        target = "expnostem/%s_issues.json/contquery.bm25.pkl" % proj
        targets.append(target)
        bm25targets.append(target)
        print "%s: expnostem" % target
        print "\tmkdir expnostem/%s_issues.json||echo done" % proj
        print "\tpython contquery.py -i data/%s_issues.json --bm25 --bp expnostem/%s_issues.json/bp.pkl --runcont --parts 100 --log expnostem/%s_issues.json/contquery.bm25.log --contquerypkl expnostem/%s_issues.json/contquery.bm25.pkl --nostem" % (proj, proj, proj, proj)

        

        # notcont
        target = "expnostem/%s_issues.json/notcontquery.pkl" % proj
        targets.append(target)
        nottargets.append(target)
        print "\n%s: expnostem" % target
        print "\tmkdir expnostem/%s_issues.json||echo done" % proj
        print "\tpython contquery.py -i data/%s_issues.json --bp expnostem/%s_issues.json/nbp.pkl --runcont --parts 100 --log expnostem/%s_issues.json/notcontquery.log --contquerypkl expnostem/%s_issues.json/notcontquery.pkl --nostem --notcont" % (proj, proj, proj, proj)

        # nwords (25)
        target = "expnostem/%s_issues.json/nwordscontquery.pkl" % proj
        targets.append(target)
        nottargets.append(target)
        print "\n%s: expnostem" % target
        print "\tmkdir expnostem/%s_issues.json||echo done" % proj
        print "\tpython contquery.py -i data/%s_issues.json --bp expnostem/%s_issues.json/nwordsbp.pkl --runcont --parts 100 --log expnostem/%s_issues.json/nwordscontquery.log --contquerypkl expnostem/%s_issues.json/nwordscontquery.pkl --nostem --nwords" % (proj, proj, proj, proj)


        # bp notcont
        target = "expnostem/%s_issues.json/notcontquery.bugparty.pkl" % proj
        targets.append(target)
        notbptargets.append(target)
        print "\n%s: expnostem" % target
        print "\tmkdir expnostem/%s_issues.json||echo done" % proj
        print "\tpython contquery.py --bugparty -i data/%s_issues.json --bp expnostem/%s_issues.json/nbp.pkl --runcont --parts 100 --log expnostem/%s_issues.json/notcontquery.bugparty.log --contquerypkl expnostem/%s_issues.json/notcontquery.bugparty.pkl --nostem --notcont" % (proj, proj, proj, proj)

        # bm25 notcont
        target = "expnostem/%s_issues.json/notcontquery.bm25.pkl" % proj
        targets.append(target)
        notbm25targets.append(target)
        print "\n%s: expnostem" % target
        print "\tmkdir expnostem/%s_issues.json||echo done" % proj
        print "\tpython contquery.py --bm25 -i data/%s_issues.json --bp expnostem/%s_issues.json/nbp.pkl --runcont --parts 100 --log expnostem/%s_issues.json/notcontquery.bm25.log --contquerypkl expnostem/%s_issues.json/notcontquery.bm25.pkl --nostem --notcont" % (proj, proj, proj, proj)
        
        #titles
        target = "expnostem/%s_issues.json/titlecontquery.pkl" % proj
        targets.append(target)
        titletargets.append(target)
        print "%s: expnostem" % target
        print "\tmkdir expnostem/%s_issues.json||echo done" % proj
        print "\tpython contquery.py -i data/%s_issues.json --bp expnostem/%s_issues.json/bp.pkl --runcont --parts 100 --log expnostem/%s_issues.json/titlecontquery.log --contquerypkl expnostem/%s_issues.json/titlecontquery.pkl --justtitles --nostem" % (proj, proj, proj, proj)

        
for proj in projs:
        print "%s: expnostem/%s_issues.json/contquery.pkl exp1/%s_issues.json/contquery.pkl" % (proj,proj,proj)
        print "\t"
        print "not%s: expnostem/%s_issues.json/notcontquery.pkl exp1/%s_issues.json/notcontquery.pkl" % (proj,proj,proj)
        print "\t"
print "\n"
print "NOT: %s" % " ".join(nottargets)
print "\n"
print "BP: %s" % " ".join(bptargets)
print "\n"
print "NOTBP: %s" % " ".join(notbptargets)
print "\n"
print "BM25: %s" % " ".join(bm25targets)
print "\n"
print "NOTBM25: %s" % " ".join(notbm25targets)
print "\n"
print "TITLE: %s" % " ".join(titletargets)
print "\n"
print "ALL: %s" % " ".join(targets)

print "Makefile: genmakefile.py\n\tpython genmakefile.py > Makefile"
