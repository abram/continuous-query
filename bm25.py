import json
import gensim.summarization.bm25 as bm25
import ireval
import IRExp
import gensim
from gensim.similarities import MatrixSimilarity
import numpy as np

query = u'Multilanguage Support\nPlease allow to put non ascii characters (Greek\nletters, french etc.) to labels, text boxes and\n'
dupes = [122]
descs = json.load(file("bm25-descs.txt"))


def noneTo(v,out=[]):
   if v == None:
      return out
   return v


bm25.PARAM_K1 = 1.25
bm25.PARAM_B  = 0.75
bm25.EPSILON  = 0.25

irmodel = IRExp.IRExp({
    "word_filter":IRExp.WordFilter(),
    "stemmer":IRExp.Stemmer(),
    "char_filter":IRExp.AllCharCharFilter(),
    "digit_filter":IRExp.StripDigits(),
    "caser":IRExp.LowerCaser()
})
print(irmodel.stemmer)
irmodel.build_corpus(descs)
words = irmodel.get_words()
dictionary = gensim.corpora.Dictionary([words])
corpus = [dictionary.doc2bow(noneTo(doc)) for doc in irmodel.get_corpus() if doc is not None]
bm25c = bm25.BM25(corpus)
avg_idf = sum(float(val) for val in bm25c.idf.values()) / len(bm25c.idf)
print("idf len %s" % len(bm25c.idf))
print("dictionary len %s" % len(dictionary))
print("irmodel get words %s" % len(irmodel.get_words()))

print("len %s " % len(bm25c.corpus))
print("avg_idf %s" % avg_idf)
def get_words(doc):
    return dictionary.doc2bow(noneTo(irmodel.process_document(doc)))



scores = bm25c.get_scores(get_words(query),avg_idf)
scu = np.array(scores)
sc = scu.argsort()[::-1]

# d7 = set(irmodel.process_document(descs[7]))
# d122 = set(irmodel.process_document(descs[122]))
# dq = set(irmodel.process_document(query))


# TFIDF Query

tfidf = gensim.models.tfidfmodel.TfidfModel(corpus)
tfidfcorpus = [tfidf[doc] for doc in corpus]

index = MatrixSimilarity(tfidfcorpus, num_features=dictionary.num_pos)



tqu = index[get_words(query)]
tq = tqu.argsort()[::-1]

# 122 should be in the top 10 for any reasonable query ssystem
assert(122 in sc[0:10])
print(sc)
print(scu[122])
assert(122 in tq[0:10])
print(tq)
print(tqu[122])




k1s = [0.0,0.25,0.5,0.75,1.0,1.25,1.5,1.75,2.0]
bs  = [0.0,0.25,0.5,0.75,1.0]
epss  = [0.0,0.1,0.2,0.25,0.3,0.4,0.5,0.75,1.0]

def runBM25(k,b,eps):
    bm25.PARAM_K1 = k
    bm25.PARAM_B  = b
    bm25.EPSILON  = eps    
    bm25c = bm25.BM25(corpus)
    avg_idf = sum(float(val) for val in bm25c.idf.values()) / len(bm25c.idf)
    scores = bm25c.get_scores(get_words(query),avg_idf)
    scu = np.array(scores)
    sc = scu.argsort()[::-1]
    index = np.arange(len(sc))[sc == 122][0]
    return index
    # print("index: %s k: %s b: %s eps: %eps" % (index, k,b,eps))

res = [(runBM25(k,b,eps),k,b,eps) for k in k1s for b in bs for eps in epss]
res2 = sorted(res, key=lambda res: -1*res[0])
for r in res2:
    print r
            
