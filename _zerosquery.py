import projects
import pickle
expr = "exp1"
expr = "expnostem"
def doit(expr):
    query_type = 'aveP-TOP5'
    query_type = 'TOP5'
    what = list()
    tzerod = 0
    tnotzero = 0 
    for project in projects.Projects().projects:
    	x = pickle.load(file("%s/%s_issues.json/contquery.pkl" % (expr,project)))
    	notzero = sum([y[query_type] != 0.0 for z in x["output"] for y in z['res']] )
    	zerod   = sum([y[query_type] == 0.0 for z in x["output"] for y in z['res']] )
    	tnotzero += notzero
    	tzerod += zerod
    	v = zerod/float(notzero + zerod)
    	what.append((project,v))
    
    print "\n".join([str(x) for x in what])
    print (tnotzero,tzerod,tzerod/float(tzerod + tnotzero), \
                           tnotzero/float(tzerod + tnotzero))

doit("exp1")
doit("expnostem")
