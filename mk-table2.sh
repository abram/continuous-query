#!/bin/bash
mv averages.csv 'averages.csv.`isodate`'
python avgs.py
mv averages.csv averages.cont.plain.csv
mv averages.tex averages.cont.plain.tex
python avgs.py --suffix .notcont exp{1,nostem}/*/notcontquery.pkl
mv averages.csv averages.notcont.plain.csv
mv averages.tex averages.notcont.plain.tex
#python avgs.py 
ls -bw1 exp{1,nostem}/*/contquery.bugparty.pkl | sort | xargs python avgs.py --suffix .bugparty
mv averages.csv averages.cont.bugparty.csv
mv averages.tex averages.cont.bugparty.tex
#python avgs.py exp{1,nostem}/*/notcontquery.bugparty.pkl
ls -bw1 exp{1,nostem}/*/notcontquery.bugparty.pkl | sort | xargs python avgs.py --suffix .bugparty.notcont
mv averages.csv averages.notcont.bugparty.csv
mv averages.tex averages.notcont.bugparty.tex
python avgs.py exp{1,nostem}/*/contquery*.bm25*pkl --suffix .bm25
mv averages.csv averages.cont.bm25.csv
mv averages.tex averages.cont.bm25.tex
python avgs.py exp{1,nostem}/*/notcontquery*.bm25*pkl --suffix .bm25.notcont
mv averages.csv averages.notcont.bm25.csv
mv averages.tex averages.notcont.bm25.tex
python avgs.py exp{1,nostem}/*/titlecontquery.pkl --suffix .title
mv averages.csv averages.cont.title.csv
mv averages.tex averages.cont.title.tex
python avgs.py exp{1,nostem}/*/nwordscontquery.pkl --suffix .nwords
mv averages.csv averages.nwords.plain.csv
mv averages.tex averages.nwords.plain.tex

# table 2
cut -d"&" -f1-3 averages.notcont.plain.tex  > _ancp.tex
cut -d"&" --complement -f1-2 averages.cont.plain.tex > _acp.tex
paste --delimiters="|" _ancp.tex  _acp.tex | sed -e 's/|/\&\t\t/' > table2.1.tex
(
	head -n 1 table2.1.tex
	tail -n +2 table2.1.tex  | head -n -3 | sed -e 's/Stemming/AStemming/' |  sort | sed -e 's/AStemming/Stemming/'|sed -e 's/For Android//'
	echo "\\hline"
	tail -n 3 table2.1.tex | sed -e 's/^/Average/'
) > table2.tex

# table 3

cut -d"&" -f1-3 averages.notcont.bugparty.tex  > _ancbp.tex
cut -d"&" --complement -f1-2 averages.cont.bugparty.tex > _acbp.tex
paste --delimiters="|" _ancbp.tex  _acbp.tex | sed -e 's/|/\&\t\t/' > table3.1.tex
(
	head -n 1 table3.1.tex
	tail -n +2 table3.1.tex  | head -n -3 | sed -e 's/Stemming/AStemming/' |  sort | sed -e 's/AStemming/Stemming/'|sed -e 's/For Android//'
	echo "\\hline"
	tail -n 3 table3.1.tex | sed -e 's/^/Average/'
) > table3.tex

# table 4

cut -d"&" -f1-3 averages.notcont.bm25.tex  > _ancbm25.tex
cut -d"&" --complement -f1-2 averages.cont.bm25.tex > _acbm25.tex
paste --delimiters="|" _ancbm25.tex  _acbm25.tex | sed -e 's/|/\&\t\t/' > table4.1.tex
(
	head -n 1 table4.1.tex
	tail -n +2 table4.1.tex  | head -n -3 | sed -e 's/Stemming/AStemming/' |  sort | sed -e 's/AStemming/Stemming/' | sed -e 's/For Android//'
	echo "\\hline"
	tail -n 3 table4.1.tex | sed -e 's/^/Average/'
) > table4.tex



# table 25

cut -d"&" -f1-3 averages.notcont.plain.tex  > _ancp.tex
cut -d"&" --complement -f1-2 averages.nwords.plain.tex > _anwp.tex
paste --delimiters="|" _ancp.tex  _anwp.tex | sed -e 's/|/\&\t\t/' > table25.1.tex
(
	head -n 1 table25.1.tex
	tail -n +2 table25.1.tex  | head -n -3 | sed -e 's/Stemming/AStemming/' |  sort | sed -e 's/AStemming/Stemming/' | sed -e 's/For Android//'
	echo "\\hline"
	tail -n 3 table25.1.tex | sed -e 's/^/Average/'
) > table25.tex
