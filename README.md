

IR Chain:

* Special Characters
  Remove/Include

* Digits
  Remove/Include

* Stop Word Removal

* Casing
  - lowercase on/off

* Stemming
  - porter stemmer on/off

* Distance
  - Cosine

* Term
  - Boolean
  - tf
  - tf-idf

* Term Filtering
  - none
  - idf


## Related Work

* https://www.researchgate.net/profile/Annibale_Panichella/publication/291495371_Parameterizing_and_Assembling_IR-based_Solutions_for_SE_Tasks_using_Genetic_Algorithms/links/56a3510108aeef24c589950a.pdf
