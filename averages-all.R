#v <- read.csv("averages-all.csv",header=T)
v <- read.csv("averages.cont.plain.csv",header=T)
plain <- v
title <- read.csv("titleaverages.csv",header=T)
bm25 <- read.csv("averages.cont.bm25.csv",header=T)
plain <- plain[order(plain$project,plain$exp),]
title <- title[order(title$project,title$exp),]


vbp <- read.csv("averages.cont.bugparty.csv",header=T)
v <- rbind(v,vbp)
v$bp <- FALSE
v$bp[grep("bugparty",as.character(v$filename))] <- TRUE
v$stemming <- TRUE
v$stemming[grep("expnostem",as.character(v$filename))] <- FALSE
v <- v[order(v$project,v$bp),]
all(v$project[!v$bp] == v$project[v$bp])


# XABBA: BP Versus Gensim

wilcox.test(v$aveP.TOP5[v$bp],v$aveP.TOP5[!v$bp],paired=TRUE)
# Different
# 	Wilcoxon signed rank test
# 
# data:  v$aveP.TOP5[v$bp] and v$aveP.TOP5[!v$bp]
# V = 256, p-value = 0.00158
# alternative hypothesis: true location shift is not equal to 0
# 



bps     = v$aveP.TOP5[v$bp]
gensims = v$aveP.TOP5[!v$bp]

library(boot)
N = 100000
bp     = numeric(N)
gensim =  numeric(N)
for ( i in 1:N )
{
    bp[i] = mean(sample(bps,size=length(bps),replace=TRUE))
    gensim[i] = mean(sample(gensims,size=length(gensims),replace=TRUE))
}
boots = gensim - bp
quantile(boots,c(0.05,0.95))
#          5%          95% 
# -0.074830030  0.002630802 

# XACKA
# paired                                        # paired
data = gensims - bps
boots = numeric(N)
for ( i in 1:N )
{
    boots[i] = mean(sample(data,size=length(data),replace=TRUE))
}
quantile(boots,c(0.05,0.95))
# > quantile(boots,c(0.05,0.95))
#          5%         95% 
# -0.05371389 -0.02007478 



                                        # what about stemming
stems = v$aveP.TOP5[v$bp & v$stemming]
nostems = v$aveP.TOP5[v$bp & (!v$stemming)]
all(v$project[v$bp & v$stemming] == v$project[(!v$bp) & v$stemming])
diff = nostems - stems

my.mean = function(x, indices) {
    return( mean( diff[indices] ) )
}
boots = boot(diff, my.mean, N)
boot.ci(boots)
# 
# 
# > boot.ci(boots)
# BOOTSTRAP CONFIDENCE INTERVAL CALCULATIONS
# Based on 100000 bootstrap replicates
# 
# CALL : 
# boot.ci(boot.out = boots)
# 
# Intervals : 
# Level      Normal              Basic         
# 95%   (-0.0061,  0.0208 )   (-0.0041,  0.0224 )  
# 
# Level     Percentile            BCa          
# 95%   (-0.0077,  0.0188 )   (-0.0133,  0.0168 )  
# Calculations and Intervals on Original Scale
# Warning message:
# In boot.ci(boots) : bootstrap variances needed for studentized intervals
# >

wilcox.test(stems,nostems,paired=TRUE)
# 
# 	Wilcoxon signed rank test
# 
# data:  stems and nostems
# V = 17, p-value = 0.09229
# alternative hypothesis: true location shift is not equal to 0
# 
plain<- plain[order(plain$project,plain$exp),]
bm25<- bm25[order(bm25$project,bm25$exp),]
all(bm25$project == plain$project)
wilcox.test(bm25$aveP.TOP5,plain$aveP.TOP5,paired=TRUE)
wilcox.test(bm25$aveP.TOP5[plain$exp == "NoStem"],plain$aveP.TOP5[plain$exp == "NoStem"],paired=TRUE)
wilcox.test(bm25$aveP.TOP5[! plain$exp == "NoStem"],plain$aveP.TOP5[! plain$exp == "NoStem"],paired=TRUE)

data = plain$aveP.TOP5 - bm25$aveP.TOP5 
boots = numeric(N)
for ( i in 1:N )
{
    boots[i] = mean(sample(data,size=length(data),replace=TRUE))
}
quantile(boots,c(0.05,0.95))

data = bm25$i1stTOP5  - plain$i1stTOP5
boots = numeric(N)
for ( i in 1:N )
{
    boots[i] = mean(sample(data,size=length(data),replace=TRUE))
}
quantile(boots,c(0.05,0.95))

bootDiff <- function(a,b,n=N) {
    data = a - b
    boots = numeric(n)
    for ( i in 1:n )
    {    
        boots[i] = mean(sample(data,size=length(data),replace=TRUE))
    }
    quantile(boots,c(0.05,0.95))
}

print("Titles versus no titles")
wilcox.test(plain$aveP.TOP5,title$aveP.TOP5,paired=TRUE)
bootDiff(plain$aveP.TOP5,title$aveP.TOP5)
bootDiff(plain$aveP.TOP5[ chooseStemming ],title$aveP.TOP5[ chooseStemming])
bootDiff(plain$aveP.TOP5[!chooseStemming ],title$aveP.TOP5[!chooseStemming])

wilcox.test(plain$i1stTOP5           ,title$i1stTOP5,paired=TRUE)
bootDiff(title$i1stTOP5,plain$i1stTOP5)
bootDiff(title$first.TOP5, plain$first.TOP5)


print("Differences in performance per project titles or not")
x <- c()
x$project = title$project
x$exp = title$exp
# x$diffi = title$i1stTOP5 - plain$i1stTOP5
x$t = title$i1stTOP5
x$p = plain$i1stTOP5
x$diff = title$i1stTOP5 - plain$i1stTOP5
x$diff2 = abs(x$diff) > 0.5 # diff of 0.5 words
data.frame(x)
mean(x$diff)
wilcox.test(x$t,x$p,paired=TRUE)
print("Is stemming for titles meaningful?")
bootDiff(title$aveP.TOP5[ chooseStemming ],title$aveP.TOP5[! chooseStemming])
wilcox.test(title$aveP.TOP5[ chooseStemming ],title$aveP.TOP5[! chooseStemming],paired=TRUE)

x <- c()
x$project = title$project[ chooseStemming ]
x$diff = title$i1stTOP5[chooseStemming] - title$i1stTOP5[! chooseStemming]
data.frame(x)


wilcox.test(plain$aveP.TOP5,title$aveP.TOP5,paired=TRUE)
chooseStemming <- plain$exp=="Stemming"
wilcox.test(plain$aveP.TOP5[  chooseStemming], title$aveP.TOP5[  chooseStemming],paired=TRUE)
wilcox.test(plain$aveP.TOP5[! chooseStemming], title$aveP.TOP5[! chooseStemming],paired=TRUE)

wilcox.test(plain$i1stTOP5,title$i1stTOP5,paired=TRUE)
bootDiff(plain$i1stTOP5,title$i1stTOP5)

print("XXXAAA")
print("STEMMING VERSUS NOT STEMMING AT AVEPTOP5")
wilcox.test(plain$aveP.TOP5[ chooseStemming], plain$aveP.TOP5[! chooseStemming],paired=TRUE)
bootDiff(plain$aveP.TOP5[ chooseStemming], plain$aveP.TOP5[! chooseStemming])


print("XXWEEK")
print("Is Old MAP > MAP?")
tcont <- read.csv("averages.cont.plain.csv",header=T)
tcont <- tcont[order(tcont$project,tcont$exp),]
fcont <- read.csv("averages.notcont.plain.csv",header=T)
fcont <- fcont[order(fcont$project,fcont$exp),]
types <- c("MAP","MRR","TOP1","TOP5","aveP.TOP5")
for (type in types) {
    print(paste(type))
    a = tcont[,type]
    b = fcont[,type]
    print(wilcox.test(a,b,paired=TRUE))
    print(bootDiff(a,b,))
}

# for (xp in c("Stemming","NoStem")) {
#     for (type in types) {
#         print(paste(type,xp))
#         # a = tcont[tcont$exp == "NoStem",type]
#         #b = fcont[fcont$exp == "NoStem",type]
#         a = tcont[tcont$exp == xp,type]
#         b = fcont[fcont$exp == xp,type]
#         print(wilcox.test(a,b,paired=TRUE))
#         print(bootDiff(a,b,))
#     }
# }
# 


nwords <- read.csv("averages.nwords.plain.csv",header=T)
nwords <- nwords[order(nwords$project,nwords$exp),]
fcont <- read.csv("averages.notcont.plain.csv",header=T)
fcont <- fcont[order(fcont$project,fcont$exp),]
all(nwords$project == fcont$project)
all(nwords$exp == fcont$exp)
all(nwords$exp == plain$exp)
all(nwords$project == plain$project)


print("25 words versus NotCont")
types <- c("MAP","MRR","TOP1","TOP5")
for (type in types) {
    print(paste(type))
    a = nwords[,type]
    b = fcont[,type]
    print(wilcox.test(a,b,paired=TRUE))
    print(bootDiff(a,b))
}
wilcox.test(nwords$MAP,fcont$MAP,paired=TRUE)
wilcox.test(nwords$MAP[nwords$project!="MyTrack"],fcont$MAP[nwords$project!="MyTrack"],paired=TRUE)
bootDiff(nwords$MAP[nwords$project!="MyTrack"],fcont$MAP[nwords$project!="MyTrack"]
print("Cont versus nwords")
print(wilcox.test(nwords$MAP,plain$MAP,paired=TRUE))
print(bootDiff(nwords$MAP,plain$MAP))
